import React from "react";
import MyHeader from "./MyHeader";
import MyFooter from "./MyFooter";
import Page from "./Page";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

function App() {
  return (
    <div className="App">
      <MyHeader />
      <Page />
      <MyFooter />
    </div>
  );
}

export default App;
