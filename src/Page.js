import React from "react";
import { Form, Button } from "react-bootstrap";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 5,
    };

    this.decrement = this.decrement.bind(this);
    this.icrement = this.icrement.bind(this);
    this.reset = this.reset.bind(this);
  }
  icrement(event) {
    let index = this.state.index;
    console.log(index);
    index++;
    console.log(index);
    this.setState({
      index: index,
    });
    console.log(this.state);
  }
  decrement(event) {
    let index = this.state.index;
    console.log(index);
    index = index--;
    console.log(index);
    this.setState({
      index: index,
    });
  }
  reset(event) {
    this.setState({
      index: 0,
    });
  }
  render() {
    return (
      <div className="my-page">
        <h1>page</h1>

        <Form>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Index</Form.Label>
            <Form.Control
              type="text"
              defaultValue={this.state.index}
              ref={this.input}
            />
          </Form.Group>

          <Button type="button" variant="primary" onClick={this.icrement}>
            Increment
          </Button>
          <Button type="button" variant="info" onClick={this.decrement}>
            Decrement
          </Button>
          <Button type="button" variant="danger" onClick={this.reset}>
            Reset
          </Button>
        </Form>
      </div>
    );
  }
}

export default Page;
